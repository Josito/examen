const express = require('express');

const moment = require('moment');

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;


let eventos = [
  {
    name: 'nombre',
    type: 'tipo',
    date: 'fecha',
    place: 'lugar',
    description: 'descriccion',

  }
];

const app = express();

/*
  Le estamos indicando a express que tiene que leer las variables 
  del BODY, esto es, el curso de las peticiones http que nos hagan.
  Aquí vendrá la información para el POST y el PUT.

  Esta forma de interactuar con express es un middleware. Se trata de funciones
  que se ejecutan en cada petición. En el ejemplo de CORS creamos un middleware
  desde cero
*/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


/*
  Con esta línea le estamos diciendo a express que
  sirva ficheros de la carpeta 'public'. Lo usamos
  para poder tener un servidor web que sirve una página que 
  hace peticiones a nuestra API, y no tener problemas de CORS.
  Los problemas de CORS los tuvimos cuando lanzamos un
  servidor en otro puerto con `python -m SimpleHTTPServer`
*/
app.use(express.static('public'));

/*
  Obtenemos del fichero de configuración los dominios
  a los que vamos a permitir el acceso a la API
*/
const domainCors = config.get('domainCors');


/*
  Configuración de CORS. 

  Como decíamos más arriba, esto es un middleware, es decir,
  una función que se va a ejecutar en cada petición. Puede haber 
  un número indefinido de ellos y su finalidad es modificar el contenido
  de los objetos req y res. En este caso estamos modificando el objeto
  de respuesta para que incluya cabeceras para indicarle cuáles son 
  los dominios desde los que se permite acceder a la API.

  En el caso anterior, lo que hacíamos era indicarle que leyera la
  información del body

  Notad la llamada a 'next()'. Es necesario llamar a esta función para
  encadenar todos los middlewares definidos.
*/
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", domainCors.join(','));
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});



app.get('/eventos', (req, res) => {

  res.send(eventos);
});


app.post('/eventos', (req, res) => {
  let name = req.body['name'];

  if (name === undefined) {
    res.status(400).send();
    return;
  }

  name = name.trim()

  if (name.length === 0) {
    res.status(400).send();
    return;
  }

  if (eventos[name] !== undefined) {
    res.status(409).send();
    return;
  }

  res.send();
})



const port = config.get('server.port');

app.listen(port, function () {
  logger.info(`Starting points of interest application listening on port ${port}`);
});
